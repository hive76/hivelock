# Initial setup
## Installing dependencies
In a python3 virtualenv, run:

```
pip install .
```

This will install Hivelock in this virtualenv. You can install it in editable mode for development if you'd like:

```
pip install -e .
```

## Setting up the DB
Edit `alembic.ini` and make sure the `sqlalchemy.url` connection string matches the one you'll be using in production. The default is to use an sqlite DB named `users.db` in the current directory.

```
sqlalchemy.url = sqlite:///users.db
```

Then run alembic:

```
alembic upgrade head
```

## Configuring Hivelock
Copy `settings.yaml.example` to `settings.yaml`. You may want to customize the connection string - it follows the [SQLAlchemy](http://docs.sqlalchemy.org/en/latest/core/engines.html) format.

## Adding a new user

```
hivelock add_user
```

You'll be prompted for a username, password, and hexadecimal fob ID. Enter these in, and the script will add them to the database.

## Starting Hivelock
In the same virtualenv as before, run:

```
hivelock
```

In its default configuration, the application will wait for codes on stdin for easy testing. Once the application has started, type a code and press enter to check it.
