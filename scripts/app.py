#!/usr/bin/env python3

import yaml, argparse, sys, logging, serial, time, traceback
try:
    import sentry_sdk
except ImportError:
    pass
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

from hivelock.blueprints.keyfob import Keyfob
from hivelock.blueprints.user import User
from hivelock.locks.stdout import StdoutLock

try:
    from hivelock.locks.beaglebone_gpio import GPIOLock
except ImportError:
    sys.stderr.write("Failed to load GPIO lock module - disabling\n")
try:
    from hivelock.locks.rpi_gpio import RPiGPIOLock
except ImportError:
    sys.stderr.write("Failed to load RPi GPIO lock module - disabling\n")
try:
    from hivelock.readers.rc522 import rc522
except ImportError:
    sys.stderr.write("pirc522 not found - disabling rc522 support\n")
try:
    from hivelock.readers.pn532 import pn532
except ImportError:
    sys.stderr.write("pn532 not found - disabling pn532 support\n")

class Hivelock():
    def check_code(self, code):
        """Check if the given code is in the database
        @param code: Hex string of code to check
        @return: User with given fob on their account, otherwise None
        """
        session = self.Session()
        user = session.query(User).join(Keyfob, Keyfob.user_id==User.id).filter(Keyfob.fob_id == bytes.fromhex(code)).first()
        session.close()
        return user

    def __init__(self, config):
        with open(config) as f:
            self.config = yaml.safe_load(f)

        if 'sentry_dsn' in self.config:
            sentry_sdk.init(self.config['sentry_dsn'])

        missing_keys = {'log_file', 'connection_string'}.difference(self.config)
        if(missing_keys):
            sys.stderr.write("Missing config items: {} - using defaults\n".format(str(missing_keys)))

        log_filename = self.config.get('log_file')
        if('log_level' in self.config):
            if(self.config.get('log_level') == 'info'): log_level = logging.INFO
            elif(self.config.get('log_level') == 'debug'): log_level = logging.DEBUG
            else: log_level = logging.INFO
        else:
           log_level = logging.INFO
        if(log_filename):
            logging.basicConfig(filename=log_filename, level=log_level)
        else:
            logging.basicConfig(level=log_level)

        connection_string = self.config.get('connection_string', 'sqlite:///:memory:')
        engine = create_engine(connection_string)
        self.Session = sessionmaker(bind=engine)

    def add_user(self, username, fullname, code):
        session = self.Session()
        fob = Keyfob(fob_id=bytes.fromhex(code))
        user = User(name=username, fullname=fullname, keyfobs=[fob])
        session.add(user)
        session.commit()

def main():
    parser = argparse.ArgumentParser(description='Hive76 Door Lock')
    parser.add_argument('cmd', type=str, nargs='?', help="interactive command (valid commands: add_user")
    parser.add_argument('--config', dest='config', type=str, help='path to the config file location',
                        default='settings.yaml')
    args = parser.parse_args()

    lock = Hivelock(config=args.config)

    if('cmd' in args and args.cmd == 'add_user'):
        print('Adding new user')
        username = input("Username: ")
        fullname = input("Full name: ")
        code = input("Keyfob ID (Hex): ")
        lock.add_user(username, fullname, code)
        sys.exit(0)

    reader = lock.config.get('reader', 'stdin')
    if(reader == 'stdin'):
        ser = sys.stdin
    elif(reader == 'serial'):
        device = lock.config.get('serial_reader')
        ser = serial.Serial(device)
    elif(reader == 'rc522'):
        ser = rc522()
    elif(reader == 'pn532'):
        ser = pn532()

    lockdev = lock.config.get('lock', 'stdout')
    platform = lock.config.get('platform', 'beaglebone')
    if(lockdev == 'stdout'):
        lockmanager = StdoutLock()
    elif(lockdev == 'gpio' and platform == 'beaglebone'):
        lockmanager = GPIOLock()
    elif(lockdev == 'gpio' and platform == 'raspberry'):
        lockmanager = RPiGPIOLock()

    time.sleep(5) # Super hacky delay before the first read

    try:
        while(True):
            try:
                cur = ser.readline() # read a line from serial port
                logging.debug('Received code {}'.format(cur.strip()))
                result = lock.check_code(cur.strip())
                if(result):
                    logging.info("Authorized - welcome {}".format(result.name))
                    lockmanager.unlock(delay=8)
                else:
                    logging.info("Invalid tag detected - serial {}".format(cur.strip()))
                    time.sleep(0.25)
            except ValueError as e:
                logging.info('Invalid hex data received from reader')
                time.sleep(0.1)
            except AttributeError as e:
                logging.info('Invalid data returned from reader - reader probably returned error')
                logging.exception(e)
                time.sleep(0.1)
            except Exception as e:
                raise(e)
                sys.exit(1)
    finally:
        if(reader == 'rc522'):
            ser.cleanup()

if(__name__ == '__main__'):
    main()
