#!/usr/bin/env python3
import yaml
from hivelock.app import Hivelock
from hivelock.blueprints.user import User
from hivelock.blueprints.keyfob import Keyfob

lock = Hivelock('settings.yaml')
session = lock.Session()

username = input("Username: ")
fullname = input("Full name: ")
code = input("Keyfob ID (Hex): ")

fob = Keyfob(fob_id=bytes.fromhex(code))
user = User(name=username, fullname=fullname, keyfobs=[fob])

session.add(user)
session.commit()

