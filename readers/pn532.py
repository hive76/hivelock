import Adafruit_PN532 as PN532
import logging
try:
    import sentry_sdk
except ImportError:
    pass

CS   = 18
MOSI = 23
MISO = 24
SCLK = 25
lock_trigger = 8

class pn532():

    def __init__(self, **kwargs):
        self.rdr = PN532.PN532(cs=18, mosi=23, miso=24, sclk=25)
        self.rdr.begin()
        self.rdr.SAM_configuration()

    def readline(self):
        logging.info("Waiting for MiFare card...")
        while True:
            uid = None
            try:
                uid = self.rdr.read_passive_target(timeout=1)
            except RuntimeError as e:
                logging.exception(e)
                if(sentry_sdk):
                    sentry_sdk.capture_exception(e)
                pass
            if uid is None:
                continue
            logging.debug('Found code {}'.format(uid))
            return uid[::-1].hex()
