#!/usr/bin/env python
# -*- mode: python -*-

class RfidReader():
    def init_reader():
        """Initialize the RFID reader. This could be setting up ports, establishing connections, etc."""
        return NotImplemented
    
    def read_tag():
        """Read a single tag from the RFID reader. Blocks until reader receives a tag."""
        return NotImplemented


