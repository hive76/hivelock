#!/usr/bin/env python3
# -*- mode: python -*-

import sys

class StdinReader(RFIDReader):
    def init_reader():
        pass
    def read_tag():
        sys.stdin.readline()
