from pirc522 import RFID
import logging

class rc522():
    def __init__(self, **kwargs):
        self.rdr = RFID(bus=1,device=0,pin_irq="P1_26",pin_rst="P1_28")
        self.rdr.set_antenna_gain(0x06)

    def readline(self):
        logging.info("Antenna gain: {}".format(self.rdr.read_antenna_gain()))
        self.rdr.wait_for_tag()
        (error, tag_type) = self.rdr.request()
        if not error:
            print("Tag detected")
            (error, uid) = self.rdr.anticoll()
            if not error:
                tag_uid = bytes(uid[:-1][::-1]).hex()
                self.rdr.stop_crypto()
                return tag_uid

    def cleanup(self):
        self.rdr.cleanup()

