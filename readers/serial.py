#!/usr/bin/env python3
# -*- mode: python -*-

import serial

class SerialReader(RFIDReader):
    reader = None
    
    def init_reader(port="/dev/ttyUSB0"):
        self.reader = serial.Serial(port)
    
    def read_tag():
        return self.reader.readline()
