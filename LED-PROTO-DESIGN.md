## Overview

The Raspberry Pi does not have the real-time I/O capabilities needed to drive our LED ring. To resolve this, it talks to a Teensy 3.0 over UART serial with a custom command set.

## [DRAFT] Hivelock LED ring serial connection protocol

The command set used to control to the Teensy is as follows:

- `0xFE 0x01 0x00-0xFF` - Set the LED brightness
- `0xFE 0x02 0x00-0xFF 0x00-0xFF 0x00-0xFF` - Set all LEDs to a given color
- `0xFE 0x03 0x00-0xFF 0x00-0xFF` - Set the delay between streamed frames in milliseconds
- `0xFE 0x04 0x00-0xFF` - Set the frame interpolation mode (see documentation for list of available modes)
- `0xFE 0xFE 0x00-0xFF` - Play a preprogrammed animation (see documentation for list of available animations)

Streaming to the teensy is slightly more involved. First, send the stream start command:

`0xFE 0xFF`

Then send the number of frames to be sent as two bytes:

`0x00-0xFF 0x00-0xFF`

Now you can start sending frames. Send a group of 32 RGB color values. For example:

```
0x10 0x20 0x30
0x40 0x50 0x60
[...]
0x12 0x34 0x56
```

You MUST then wait for the Teensy to respond with `0xFE 0xFF` before sending another frame.
