import sys, time

class StdoutLock():
    def __init__(self):
        pass
    
    def unlock(self, delay=8):
        sys.stdout.write("Unlocked...\n")
        time.sleep(delay)
        sys.stdout.write("Locked!\n")

    def cleanup(self):
        pass
