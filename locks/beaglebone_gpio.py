import time
import Adafruit_BBIO.GPIO as GPIO

class GPIOLock():
    def __init__(self, pin='P2_17'):
        GPIO.setup(pin, GPIO.OUT, pull_up_down=GPIO.PUD_DOWN)
        GPIO.output(pin, GPIO.LOW)
        self.pin = pin

    def unlock(self, delay=8):
        GPIO.output(self.pin, GPIO.HIGH)
        time.sleep(delay)
        GPIO.output(self.pin, GPIO.LOW)

    def cleanup(self, pin='P2_17'):
        GPIO.cleanup()
    
