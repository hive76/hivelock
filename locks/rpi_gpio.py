import time
import RPi.GPIO as GPIO

class RPiGPIOLock():
    def __init__(self, pin=10, led_pin=9):
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, GPIO.LOW)
        GPIO.setup(led_pin, GPIO.OUT)
        GPIO.output(led_pin, GPIO.LOW)
        self.pin = pin
        self.led_pin = led_pin

    def unlock(self, delay=8):
        GPIO.output(self.pin, GPIO.HIGH)
        GPIO.output(self.led_pin, GPIO.HIGH)
        time.sleep(delay)
        GPIO.output(self.pin, GPIO.LOW)
        GPIO.output(self.led_pin, GPIO.LOW)

    def cleanup(self, pin=10):
        GPIO.cleanup()
