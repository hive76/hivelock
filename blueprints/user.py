from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from .base import Base
from .keyfob import Keyfob

class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    fullname = Column(String)

    keyfobs = relationship('Keyfob', order_by=Keyfob.id, back_populates='user')

    def __repr__(self):
        return "<User(name='%s', fullname='%s')>" % (self.name, self.fullname)
