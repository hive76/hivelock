from sqlalchemy import ForeignKey, Column, Integer, LargeBinary
from sqlalchemy.orm import relationship
from .base import Base

class Keyfob(Base):
    __tablename__ = 'keyfobs'

    id = Column(Integer, primary_key = True)
    fob_id = Column(LargeBinary)
    user_id = Column(Integer, ForeignKey('users.id'))

    user = relationship('User', back_populates='keyfobs')

    def __repr__(self):
        return "<Keyfob(fob_id='%s')>" % self.fob_id

    def __eq__(self, other):
        return self.fob_id == other.fob_id

    def __hash__(self):
        return hash(self.fob_id)
